function cacheFunction(callback){
    let cacheobj={};

    if(typeof callback==='function'){    
        return function closurefunction(...givenObject){

            let stringArg=JSON.stringify(givenObject);
            // console.log(stringArg);
            if(cacheobj[stringArg]===undefined){
                let outputOfThis = callback(...givenObject);
                cacheobj[stringArg]= outputOfThis;
                return outputOfThis;
            }else{
               // console.log('hello');
              return cacheobj[stringArg];  
            }
        }
    }else{
        throw new Error("this is not a function");
    }
}module.exports=cacheFunction;