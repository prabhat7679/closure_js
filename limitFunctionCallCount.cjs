function limitFunctionCallCount(cb, number){
    
    let functionCallLimit=0;
    if(typeof cb==='function' && typeof number === 'number'){

        return function(...parameterpassed){
            if(functionCallLimit >= number){
            return null;
             }else{
                functionCallLimit+=1;
               return cb(...parameterpassed);
           }
       }
   }
   else{
       throw new Error('function missmatch or number missmatch');
       }
}
module.exports=limitFunctionCallCount;